trigger InStockInsert on Product__c (before insert) {
	if(Trigger.new[0] != null){ 
       	if(Trigger.new[0].Total_Inventory__c > 0){
           	Trigger.new[0].In_Stock__c = true;                        
      	}
        else{
        	Trigger.new[0].In_Stock__c = false;                       
        }       
    }
}