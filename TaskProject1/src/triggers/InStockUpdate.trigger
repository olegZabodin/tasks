trigger InStockUpdate on Product__c (before update) {
	for(Integer i=0;i<Trigger.new.size();i++){
    	if(Trigger.old[i].In_Stock__c != Trigger.new[i].In_Stock__c){
        	Trigger.new[i].addError('You cant change In_Stock__c field.');            
        }
        if(Trigger.new[i].Total_Inventory__c > 0){
 	       Trigger.new[i].In_Stock__c = true;                        
        }
        else{
    	    Trigger.new[i].In_Stock__c = false;                       
        }        
	}
}