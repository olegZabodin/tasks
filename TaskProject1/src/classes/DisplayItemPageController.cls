global class DisplayItemPageController {
    //set new value in field if it possible.
    @RemoteAction
    global static DataStructure setValue(DataStructure ds){
        try{
            for(SObject so: ds.value){
                update so;
            }
            return ds;
        }catch (Exception x){
            DataStructure dss = new DataStructure();
            dss.exceptions.add(x.getMessage());
            return dss;
        }
    }

    @RemoteAction
    global static DataStructure setNewObjectsName_Beta(String type) {
        DataStructure ds = new DataStructure();
        List<String> objList = new List<String>();
        try {
            String objName;
            for (Schema.SObjectType objTyp : Schema.getGlobalDescribe().Values()) {
                if (type == 'Custom') {
                    Schema.DescribeSObjectResult describeSObjectResultObj = objTyp.getDescribe();
                    if (describeSObjectResultObj.isCustom()) {
                        objName = objTyp.getDescribe().getName();
                    }
                } else {
                    Schema.DescribeSObjectResult describeSObjectResultObj = objTyp.getDescribe();
                    if (!describeSObjectResultObj.isCustom()) {
                        objName = objTyp.getDescribe().getName();
                    }
                }
                objList.add(objName);
            }
            ds.objsName = objList;
            return ds;
        }catch (DMLException x){
            ds.exceptions.add(x.getMessage());
            return ds;
        }
        return null;
    }
    @RemoteAction
    global static DataStructure setNewfieldsName_Beta(String selectedObjectFieldName){
        DataStructure ds = new DataStructure();
        try {
            List<String> fieldsName = new List<String>();
            SObjectType selectedField = Schema.getGlobalDescribe().get(selectedObjectFieldName);
            map<string, Schema.SObjectField> lfields = selectedField.getDescribe().fields.getMap();
            for (String key : lfields.keySet()) {
                fieldsName.add(key);
            }
            ds.fieldsName = fieldsName;
            return ds;
        }catch (DMLException e){
            ds.exceptions.add(e.getMessage());
            return ds;
        }
        return null;
    }
    @RemoteAction
    global static DataStructure displayTable_Beta(String obj, String fil){
        DataStructure ds = new DataStructure();
        ds.objName = obj;
        ds.fieldName = fil; 
        try {
            String partQueryObj;
            String partQueryFil;
            List<List<sObject>> resultList = new List<List<SObject>>();
            SObjectType selectedField = Schema.getGlobalDescribe().get(obj);
            map<string, Schema.SObjectField> lFields = selectedField.getDescribe().fields.getMap();
            List<String> lFild = new List<String>();
            for (String fild : lFields.keySet()) {
                lfild.add(fild);
            }
            for (String shemaFild : lfild) {
                if (fil == shemaFild) {
                    List<SObject> fieldValeList = DataBase.query('select ' + fil + ' from ' + obj);
                    ds.value = fieldValeList;
                    return ds;
                }
            }
        }catch(DMLException e){
            ds.exceptions.add(e.getMessage());
            return ds;
        }
        return null;
    }
    global class DataStructure{
        
        global String objName;
        global List<String> objsName;
       
        global String fieldName;
        global List<String> fieldsName; 
       
        global List<sObject> value;
        global List<String> exceptions;
        global List<String> correctionWarning;
        global DataStructure(){
            value = new List<sObject>();
            exceptions = new List<String>();
            correctionWarning = new List<String>();
            fieldsName = new List<String>();
            objsName = new List<String>();
        }
        
    }
}
